package com.iwec.hibernate.example.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import com.iwec.hibernate.example.dao.GenericDAO;
import com.iwec.hibernate.example.dao.JpaEntityManagerFactory;
import com.iwec.hibernate.example.model.Book;

public class BookDAOImpl implements GenericDAO<Book> {

	@Override
	public Book find(Integer id) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		Book result = em.find(Book.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public List<Book> list() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<Book> result = em.createQuery("from Book", Book.class)
				.getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public void save(Book t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void remove(Book t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	}

}
