package com.iwec.repo;

import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import com.iwec.model.Name;



public class nameRepository {
	
	private static final Map<String, Name> names = new TreeMap<>();
	
	@PostConstruct
	public void initData() {

		Name name = new Name();
		name.setName("Stefan");
		name.getContent();
		
		names.put(name.getName(), name);

		name = new Name();
		name.setName("Daniela");
		name.getContent();
		names.put(name.getName(), name);
	}

	
	
	
	public Name findByName(String name) {
		return (name == null) ? null : names.get(name);
	}

}
