package com.iwec.shape;

public interface ShapeFactory {

	public Shapes getShape(String shape);
}
