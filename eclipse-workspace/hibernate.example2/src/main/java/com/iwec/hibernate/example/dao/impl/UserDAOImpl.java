package com.iwec.hibernate.example.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import com.iwec.hibernate.example.dao.GenericDAO;
import com.iwec.hibernate.example.dao.JpaEntityManagerFactory;
import com.iwec.hibernate.example.model.User;

public class UserDAOImpl implements GenericDAO<User> {

	@Override
	public User find(Integer id) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		User result = em.find(User.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public List<User> list() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<User> result = em.createQuery("from User", User.class)
				.getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public void save(User t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void remove(User t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	}

}
