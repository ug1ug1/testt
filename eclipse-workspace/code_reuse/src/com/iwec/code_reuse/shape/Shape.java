package com.iwec.code_reuse.shape;

public interface Shape {
	public double calculateArea();
}