package com.iwec.junit5.examples;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AssertThatTest {
	@Test
	public void test_matcher_behavior() throws Exception {
		int myAge = 46;

		assertThat(myAge).isEqualTo(46);
		assertThat(myAge).isNotEqualTo(36);
	}

	@Test
	public void verify_Strings() throws Exception {
		String companyName = "InterWorks";
		assertThat(companyName).startsWith("Inter");
		assertThat(companyName).endsWith("Works");
		assertThat(companyName).contains("rW");
	}
}