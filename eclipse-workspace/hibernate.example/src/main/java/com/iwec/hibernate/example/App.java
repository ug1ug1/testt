package com.iwec.hibernate.example;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.iwec.hibernate.example.model.Student;

public class App {
	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		System.out.println("Starting Transaction");
		entityManager.getTransaction().begin();
		
		Student student = new Student("Круме", "Крумев");
		
		System.out.println("Saving Student to Database");

		entityManager.persist(student);
		entityManager.getTransaction().commit();
		
		System.out.println("Generated Student ID = " + student.getId());

		// get an object using primary key.
		System.out.println("Reading student from database, using primary key");

		Student stdnt = entityManager.find(Student.class, student.getId());
		System.out.format("%d: %s %s\n", stdnt.getId(), stdnt.getFirstName(), stdnt.getLastName());

		
		stdnt.setFirstName("Ставре");

		// update an entity
		entityManager.getTransaction().begin();
		System.out.println("Updating Student with ID = " + stdnt.getId());
		entityManager.persist(stdnt);
		entityManager.getTransaction().commit();


		
		
		// get all the objects from students table
		@SuppressWarnings("unchecked")
		List<Student> students = entityManager
				.createQuery("SELECT student FROM Student student").getResultList();

		
		if (students == null) {
			System.out.println("No students found.");
		} else {
			for (Student st : students) {
				System.out.format("%d: %s %s\n", st.getId(), st.getFirstName(), st.getLastName());
			}
		}
		
		
		@SuppressWarnings("unchecked")
		List<Student> krumevi = entityManager.createQuery("SELECT student from Student student where student.firstName = ?1")
			      .setParameter(1, "Круме")
			      .getResultList();
		
		krumevi.stream().forEach(System.out::println);
		
		
		
		
		// remove an entity
		entityManager.getTransaction().begin();
		System.out.println("Deleting Student with ID = " + stdnt.getId());
		//entityManager.remove(stdnt);
		entityManager.getTransaction().commit();
		
		
		

		// close the entity manager
		entityManager.close();
		entityManagerFactory.close();

	}
}