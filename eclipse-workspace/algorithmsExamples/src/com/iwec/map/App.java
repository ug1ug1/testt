package com.iwec.map;

public class App {

	public static void main(String[] args) {
		MyMap<String, Integer> map = new MyMap<String, Integer>();
		
		map.put("Круме", 11);
        map.put("Ѓоре", 21);
        map.put("Пера", 103);
        
        System.out.println(map);
        
        map.put("Ѓоре", 231);
        
        System.out.println(map);
        
        map.remove("Пера");
        
        System.out.println(map);
        
        System.out.println(map.size());
	}

}