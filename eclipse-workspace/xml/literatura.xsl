<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
  <title>Интегра Ултра</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
  <body>
  <h2>Литература:</h2>
    <table border="1">
      <tr bgcolor="#ffff00">
        <th align="left">Автор</th>
        <th align="left">Наслов</th>
		<th align="left">ISBN</th>
		<th align="left">Цена (ден.)</th>
      </tr>
      <xsl:for-each select="literatura/kniga">
      <tr>
        <td><xsl:value-of select="avtor" /></td>
        <td><xsl:value-of select="naslov" /></td>
		<td><xsl:value-of select="@isbn" /></td>
		<td><xsl:value-of select="cena" /></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>