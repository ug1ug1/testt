package com.iwec.besilak;

import java.util.Scanner;

public class Hangman {
    private static Scanner scanner = new Scanner(System.in);    
    private static String word;
    private static String availableChoices = "abcdefghijklmnopqrstuvwxyz";
    private static String hiddenWord;
    private static boolean winner = false;

    public static void main(String[] args) {
        System.out.print("Enter a word to guess: ");

        word = scanner.nextLine();
        hiddenWord = wordToQuestionMarks(word);

        System.out.println("Hangman Word Set: " + word + "\n\n");

        while (!winner) {
            guessLetter();
        }

        System.out.println("Congrats! You Win!");
    }

    private static String wordToQuestionMarks(String word) {
        return word.replaceAll(".", "?");
    }

    private static void guessLetter() {
        System.out.println("Hidden Word: " + hiddenWord);
        System.out.println("Characters to choose from: " + availableChoices);
        System.out.print("Guess a letter: ");
        String letterChoice = scanner.nextLine();

        int found = 0;

        if (hasLetter(letterChoice)) {
            found = updateGameState(letterChoice);
        }

        updateAvailableChoices(letterChoice);

        System.out.println("You found " + found + " " + letterChoice + "\n");
        gameOver();
    }

    private static int updateGameState(String letter) {
        int found = 0;

        for(int i=0; i< word.length(); i++) {
            if (word.charAt(i) == letter.charAt(0)) {
                String prev = hiddenWord.substring(0,i).concat(letter);
                hiddenWord = prev.concat(hiddenWord.substring(i+1));
                found++;
            }
        }

        return found;
    }

    private static void updateAvailableChoices(String removeLetter) {
        availableChoices = availableChoices.replace(removeLetter, " ");
    }

    private static void gameOver() {
        if (!hiddenWord.contains("?")) {
            winner = true;
        }
    }

    private static boolean hasLetter(String letter) {
        if (word.contains(letter)) {
            return true;
        }
        else {
            return false;
        }
    }

}