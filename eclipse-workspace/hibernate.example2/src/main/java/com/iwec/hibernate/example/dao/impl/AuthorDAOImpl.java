package com.iwec.hibernate.example.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import com.iwec.hibernate.example.dao.GenericDAO;
import com.iwec.hibernate.example.dao.JpaEntityManagerFactory;
import com.iwec.hibernate.example.model.Author;

public class AuthorDAOImpl implements GenericDAO<Author> {

	@Override
	public Author find(Integer id) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		Author result = em.find(Author.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public List<Author> list() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<Author> result = em.createQuery("from Author", Author.class)
				.getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public void save(Author t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void remove(Author t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	}

}
