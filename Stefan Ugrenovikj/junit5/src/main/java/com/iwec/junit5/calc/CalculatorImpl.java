package com.iwec.junit5.calc;

import org.springframework.stereotype.Component;

@Component
public class CalculatorImpl implements Calculator {

	@Override
	public int add(int x, int y) {
		long result = (long) x + (long) y;
		
		if (result > Integer.MAX_VALUE) {
			throw new RuntimeException("Overflow Exception");
		}
		
		if (result < Integer.MIN_VALUE) {
			throw new RuntimeException("UnderflowException");
		}

		return (int) result;
	}

}