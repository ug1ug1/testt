package com.iwec.snaps.writer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FileWriter {

	public static void write(String fileName, String content) {
		try (PrintWriter out = new PrintWriter(fileName)) {
			out.println(content);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
