package com.iwec.code_reuse.shape;

public class Rectangle implements Shape {
	private double a;
	private double b;

	public Rectangle(double a, double b) {
		super();
		this.a = a;
		this.b = b;
	}

	@Override
	public String toString() {
		return "Rectangle [a=" + a + ", b=" + b + "]";
	}

	@Override
	public double calculateArea() {
		return a * b;
	}

}
