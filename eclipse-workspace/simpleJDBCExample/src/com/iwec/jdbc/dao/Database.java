package com.iwec.jdbc.dao;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
	private Connection conn = null;

	public Database(String ip, int port, String dbName, String username,
			String password) {
		try {
			Class.forName("org.postgresql.Driver").getConstructor().newInstance();
			String url = "jdbc:postgresql://" + ip + ':' + port + '/' + dbName;
			conn = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insert(String firstName, String lastName) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("INSERT INTO students "
					+ "(first_name, last_name) VALUES (?, ?)");
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public String select() {
		Statement s = null;
		String result = "";
		try {
			s = conn.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM students");
			while (rs.next()) {
				result += rs.getString("first_name") + " "
						+ rs.getString("last_name") + "\n";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public void update(int id, String firstName, String lastName) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("UPDATE students SET "
					+ "first_name = ?, last_name = ? WHERE id = ?");
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.setInt(3, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void delete(int id) {
		String sql = "DELETE FROM students WHERE id = ?";
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Database db = new Database("127.0.0.1", 5432, "test", "postgres",
				"postgres");
		db.insert("Петре", "Петре�?ки");
		db.insert("�?нгеле", "�?нгеле�?ки");

		System.out.println(db.select());

		db.update(1, "Климе", "Климе�?ки");

		db.delete(2);

		System.out.println(db.select());
	}

}
