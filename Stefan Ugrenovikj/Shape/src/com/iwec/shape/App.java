package com.iwec.shape;

public class App {

	public static void main(String[] args) {
     
		ShapeFactory factory = new ShapeFactoryImpl();
		
		Shapes circle = factory.getShape("circle");
		System.out.println(circle.calculateSize());
		Shapes square = factory.getShape("square");
		System.out.println(square.calculateSize());
		Shapes triangle = factory.getShape("triangle");
		System.out.println(triangle.calculateSize());
	}

}
