package com.iudec.map.reduce;

import java.util.Arrays;
import java.util.List;

public class TaxCalculator {
	private static final float TAX = 0.18f;

	private List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);

	public void calculateWithTAX() {
		for (Integer cost : costBeforeTax) {
			double price = cost + TAX * cost;
			System.out.println(price);
		}
	}

	public void calculateWithTAXLambda() {
		costBeforeTax.stream().map(x -> x + TAX * x).forEach(System.out::println);
	}

	public double calculateTotal() {
		double total = 0;
		for (Integer cost : costBeforeTax) {
			double price = cost + TAX * cost;
			total += price;
		}
		return total;
	}

	public double calculateTotalLambda() {
		return costBeforeTax.stream().map(cost -> cost + TAX * cost).reduce((sum, cost) -> sum + cost).get();
	}

	public static void main(String[] args) {
		TaxCalculator taxCalculator = new TaxCalculator();

		System.out.println("Calculation without lambda: ");
		taxCalculator.calculateWithTAX();

		System.out.println("\nCalculation with lambda: ");
		taxCalculator.calculateWithTAXLambda();

		System.out.println("\nTotal : " + taxCalculator.calculateTotal());
		System.out.println("Total (calculation with lambda) : " + taxCalculator.calculateTotalLambda());

	}

}
