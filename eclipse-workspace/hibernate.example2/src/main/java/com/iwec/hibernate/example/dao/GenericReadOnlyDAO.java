package com.iwec.hibernate.example.dao;

import java.util.List;

public interface GenericReadOnlyDAO<T> {
	public T find(Integer id);
	public List<T> list();
}