package com.iwec.quicksort;

import com.iwec.linkedlist.LinkedList;

public class App {

	public static void main(String[] args) {
		QuickSorter sorter = new QuickSorter();

		int[] array = {10, 80,55, 65, 888, 353, 7222,23212,14,212423, 30, 90, 40, 50, 70};
		LinkedList<Integer> list = new LinkedList<>();
		System.out.println(list);

		list.addAtHead(11);
		list.addAtHead(122);
		list.addAtHead(1);
		list.addAtHead(15);
		list.addAtHead(252);
		list.addAtHead(32);
		System.out.println(list);
		

		for (int i : array) {
			System.out.print(i + " ");
		}

		sorter.quickSort(list, 0, array.length - 1);

		System.out.println();
		for (int i : array) {
			System.out.print(i + " ");
		}
	}

}
