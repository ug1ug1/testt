package com.iwec.payments.model;

public class Student implements Comparable<Student> {
	private String firstName;
	private String lastName;
	private Integer payment;

	public Student() {
		super();
	}

	public Student(String firstName, String lastName, Integer payment) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.payment = payment;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getPayment() {
		return payment;
	}

	public void setPayment(Integer payment) {
		this.payment = payment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((payment == null) ? 0 : payment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (payment == null) {
			if (other.payment != null)
				return false;
		} else if (!payment.equals(other.payment))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Student [fn=" + firstName + ", ln=" + lastName + ", payment=" + payment + "]\n";
	}

	@Override
	public int compareTo(Student that) {
		return Integer.compare(that.getPayment(), this.getPayment());
	}

}
