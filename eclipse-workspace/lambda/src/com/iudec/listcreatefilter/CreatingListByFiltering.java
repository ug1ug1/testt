package com.iudec.listcreatefilter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CreatingListByFiltering {

	private static final List<String> original = Arrays.asList("Java", "Scala", "C++", "Delphy", "Pascal");

	public List<String> filterList(int numberOfLetters) {
		return original.stream().filter(x -> x.length() > numberOfLetters).collect(Collectors.toList());
	}

	public static void main(String[] args) {
		CreatingListByFiltering clf = new CreatingListByFiltering();
		System.out.printf("Original List : %s, filtered list : %s %n", original, clf.filterList(4));
	}

}
