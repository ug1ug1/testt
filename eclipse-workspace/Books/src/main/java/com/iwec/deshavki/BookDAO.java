package com.iwec.deshavki;

import java.util.List;

import javax.persistence.EntityManager;

import com.iwec.dao.GenericDao;
import com.iwec.dao.JpaEntityManager;
import com.iwec.model.Book;

public class BookDAO implements GenericDao<Book> {

	public Book find(Integer id) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		Book result = em.find(Book.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	public List<Book> list() {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		List<Book> result = em.createQuery("from Book", Book.class)
				.getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	
	public void save(Book t) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	public void remove(Book t) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	}

}
