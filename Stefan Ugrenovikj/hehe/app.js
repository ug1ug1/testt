let request = require('request');
let express = require("express");
let app = express();
app.set('port', process.env.PORT || 4000);
 
app.get("/", function(req, res) {
	request({
		method : 'GET',
		uri : 'http://airquality.moepp.gov.mk/graphs/site/pages/MakeGraph.php?graph=StationLineGraph&station=Kumanovo&parameter=PM10&endDate=2016-11-11&timeMode=Week&background=false&i=1478861963021&lang=mk',
		"json": "true",
		"headers": {
		"Content-Type": "application/json"
		}
	};, function(error, response, body) {
		if (error) {
			throw error;
		}
		
		let result = JSON.parse(body);
		res.json(result);
	});
});
 
app.listen(app.get('port'));
console.log(`Server is listening at port ${app.get('port')}.`)