package com.iwec.collections;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class App {

	// java App 1 2 -3
	public static void main(String[] args) {
		List<Integer> numbers = new LinkedList<>();
		Set<Integer> numberSet = new TreeSet<>();

		for (String string : args) {
			Integer number = MyWrapper.string2Int(string);
			if (number != null) {
				numbers.add(number);
				numberSet.add(number);
			}
		}
		// 0 -> 33
		// 1 -> 37
		// ...
		
		System.out.println("set: " + numberSet);
		
		System.out.println(numbers);
		MyWrapper.myFilter(numbers);
		System.out.println(numbers);
		
	}

}
