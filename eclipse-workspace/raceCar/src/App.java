import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import Car.iwedu.Car;
import carReader.Reader;

public class App {

	public static void main(String[] args) {

		String fileName = "cars.csv";
		Reader reader = new Reader();
		List<Car> cars = reader.readCar(fileName);
		System.out.println(cars);

		int round = Integer.parseInt(args[0]);
		System.out.println(round);

		for (int i = 0; i < round; i++) {
			for (int j = 0; j < cars.size(); j++) {
				Random random = new Random();
				int timeLap = random.nextInt((180 - 110) + 1) + 110;
				cars.get(j).setTimePerLap(timeLap);
			}
			System.out.println(cars);
		}

		Collections.sort(cars, new Comparator<Car>() {
			public int compare(Car car1, Car car2) {
				return Integer.compare(car1.getTimePerLap(), car2.getTimePerLap());
			}
		});
		System.out.println();
	System.out.println(cars);
	}
}
