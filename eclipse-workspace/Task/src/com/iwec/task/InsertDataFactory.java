package com.iwec.task;

public interface InsertDataFactory {
	

	public String insertName();
	public String insertLastName();
	public String insertEMail();
	public String insertBirthDate();

}
