package com.iudec.sublist;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SubListExtractor {

	public List<Integer> calculateSquareFromDistinctValues(List<Integer> numbers) {
		return numbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
	}

	public static void main(String[] args) {
		List<Integer> listWithDuplicates = Arrays.asList(9, 10, 3, 4, 7, 3, 4);
		List<Integer> distinctList = new SubListExtractor().calculateSquareFromDistinctValues(listWithDuplicates);
		System.out.printf("Original List : %s,\nSquare Without duplicates : %s %n", listWithDuplicates, distinctList);
	}

}
