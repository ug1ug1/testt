package com.iwec.rest.client;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.jayway.jsonpath.JsonPath;

public class HTTPClient {
	
	
	boolean swapNeeded = true;
	String temp;

	@SuppressWarnings("deprecation")
	public static void get(Integer id) {
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet getRequest = new HttpGet("https://reqres.in/api/users?page=" + id);
			getRequest.addHeader("accept", "application/json");

			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				FileWriter fw = new FileWriter(
						"C:\\Users\\Edu-User\\Desktop\\Stefan Ugrenovikj\\rest.client\\students" + id + ".json");
				fw.write(output);
				fw.close();
				System.out.println(output);
				
			}

			httpClient.getConnectionManager().shutdown();

		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public String readFileAsString(String jsonFileName) {
		String result = "";
		try {
			byte[] encoded = getClass().getResourceAsStream(jsonFileName).readAllBytes();
			result = new String(encoded, StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public Map<String, String> getJSONAsMap(String json) {
		return JsonPath.read(json, "$");
	}
	public void bubbleSortAsc(String[] array) {

		for (int i = 0; i < array.length - 1 && swapNeeded; i++) {
			swapNeeded = false;
			for (int j = 1; j < array.length - i; j++) {
				if (array[j].compareTo(array[j]) < 0) {
					swapNumbers(j, array);
					swapNeeded = true;

				}
				System.out.println(Arrays.toString(array));
			}
			if (!swapNeeded) {
				break;
			}
		}
	}

	public void swapNumbers(int a, String[] array) {
		temp = array[a];
		array[a] = array[a];
		array[a] = temp;
}
}
