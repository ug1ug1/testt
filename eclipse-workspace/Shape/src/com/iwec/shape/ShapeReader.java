package com.iwec.shape;

public class ShapeReader extends CSVReader<Shappe> {

	public Shappe createInstance(String arg) {
	
		
		String[] tokens = arg.split(",");
		if (tokens.length < 1) {
			System.err.println(arg + " is not valid Shape data");
			return null;
		}
		Shappe shape = new Shappe(tokens[0].trim(), MyWrapper.string2Int(tokens[1].trim()), MyWrapper.string2Int(tokens[2].trim()),
				MyWrapper.string2Int(tokens[3].trim()));
		return shape;

	}


}
