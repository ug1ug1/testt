package com.iwec.code_reuse.shape;

public class Circle implements Shape {

	private double r;

	public Circle(double r) {
		super();
		this.r = r;
	}

	@Override
	public String toString() {
		return "Circle [r=" + r + ", calculateArea()=" + calculateArea() + "]";
	}

	@Override
	public double calculateArea() {
		return Math.pow(r, 2) * Math.PI;
	}

}
