package com.iwec.snaps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SnapsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SnapsApplication.class, args);

		new SnapFacade().doSnaps();

	}

}
