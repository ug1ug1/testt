let rp = require('request-promise');

let options = {
	"method":"GET", 
    "uri": "https://api.openweathermap.org/data/2.5/weather?id=792578&units=metric&appid=da03752724cff1d61d357d885dd96f2e",
    "json": "true",
	"headers": {
        "Content-Type": "application/json"
    }
};
 
rp(options)
    .then(function (result) {
		// took only main from the result
        console.log('result: ', result.main);
    })
    .catch(function (err) {
        // API call failed...
		console.log('Error while retrieving reauest: ' + err);
    });

	