package com.iudec.forachelement;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringConvertor {

	public String convertAllListElementsToString(List<String> original) {
		return original.stream().map(x -> x.toUpperCase()).collect(Collectors.joining(", "));
	}

	public static void main(String[] args) {
		List<String> languages = Arrays.asList("Java", "Scala", "C++", "Delphy", "Pascal");

		System.out.println(new StringConvertor().convertAllListElementsToString(languages));
	}
}
