package com.iwec.recursion;

public class App {

	public static void main(String[] args) {
		RecursiveCalculator calc = new RecursiveCalculator();

		System.out.println(calc.fibonacci(8));
		
		System.out.println(calc.sum(10));
		System.out.println(calc.tailSum(0, 10));
	}
}
