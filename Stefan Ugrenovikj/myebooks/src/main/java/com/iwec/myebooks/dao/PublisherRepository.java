package com.iwec.myebooks.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.iwec.myebooks.model.Publisher;

@Repository
public interface PublisherRepository extends CrudRepository<Publisher, Integer> {

}
