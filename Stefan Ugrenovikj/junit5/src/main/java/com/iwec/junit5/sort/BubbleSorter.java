package com.iwec.junit5.sort;

import java.util.List;

public abstract class BubbleSorter<T> {

	public List<T> sort(List<T> list) {

		int listSize = list.size();
		boolean isSorted;

		for (int i = 0; i < listSize; i++) {
			isSorted = true;
			for (int j = 1; j < (listSize - i); j++) {
				if (compare(list.get(j - 1), list.get(j)) > 0) {
					swap(list, j - 1, j);
					isSorted = false;
				}
			}

			// if sorted, then break, and avoid useless loop
			if (isSorted) {
				break;
			}
		}
		return list;
	}

	private void swap(List<T> list, int x, int y) {
		T temp = list.get(x);
		list.set(x, list.get(y));
		list.set(y, temp);
	}

	/**
	 * Determines if the swap of the two elements is needed.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public abstract int compare(T x, T y);
}
