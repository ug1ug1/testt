package com.iwec.junit5.examples.mockito;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.iwec.junit5.calc.CSVReader;

public class Mockito {

	
	@Mock
	CSVReader reader;
	    private boolean linesCorrectInitialized;

	    public CSVReaderMock()
	    {
	        reader = mock(CSVReader.class);
	    }

	    public CSVReaderMock returnLines(List<String> lines) {
	        // the last value has to be null
	        lines.add(null);
	        try {
	            for (String line : lines) {
	                String[] lineArr = null;
	                if (line != null) {
	                    lineArr = line.split(",");
	                }
	                when(reader.readNext()).thenReturn(lineArr);
	            }
	            linesCorrectInitialized = true;
	        } catch (IOException e) {
	            e.printStackTrace();
	        };
	        return this;
	    }

	    public CSVReader create() {
	        if (!linesCorrectInitialized) { throw new RuntimeException("lines are not initialized correct"); }
	        return reader;
	    }
}
}
