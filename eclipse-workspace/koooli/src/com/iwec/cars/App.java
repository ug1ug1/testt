package com.iwec.cars;

public class App {

	public static void main(String[] args) {
        Car myCar = new Car();
        myCar.setManufacturer("Dacia");
        myCar.setModel("Sandero");
        myCar.setProdYear(2013);
        
        System.out.println("M car is manufactured by %s, it's model is %s, and is produced at %d year.",
         myCar.getManufacturer(), myCar.getModel(), myCar.getProdYear());
	}

}
