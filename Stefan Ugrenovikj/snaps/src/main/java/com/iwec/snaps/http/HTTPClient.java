package com.iwec.snaps.http;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwec.snaps.model.StudentDTO;
import com.iwec.snaps.model.StudentPage;
import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;

@Component
public class HTTPClient {
	
	// TODO use if you want to work with Java objects
	public List<StudentDTO> getStudentsPerPage(String url) {
		StudentPage studentPage = new HTTPClient().fetchStudentPage(url);
		return studentPage.getData();
	}

	private StudentPage fetchStudentPage(String url) {
		try {
			HttpClient client = HttpClients.createDefault();
			HttpGet request = new HttpGet(url);
			request.setHeader("Accept", "application/json");
			HttpResponse response = client.execute(request);
			System.out.println(response);
			HttpEntity entity = response.getEntity();
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return mapper.readValue(entity.getContent(), StudentPage.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	// TODO make it more safe, catch possible exceptions
	public JSONArray getStudentsAsJSON(String url) {
		String jsonAsString = new HTTPClient().fetchAsJSON(url);
		return JsonPath.read(jsonAsString,
				"$.data[*].['first_name','last_name','email']");
	}
	

	private String fetchAsJSON(String url) {
		try {
			HttpClient client = HttpClients.createDefault();
			HttpGet request = new HttpGet(url);
			request.setHeader("Accept", "application/json");
			HttpResponse response = client.execute(request);
			System.out.println(response);
			HttpEntity entity = response.getEntity();
			return EntityUtils.toString(entity);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}