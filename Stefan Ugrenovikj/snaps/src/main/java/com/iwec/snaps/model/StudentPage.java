package com.iwec.snaps.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentPage {

	private Integer page;

	private List<StudentDTO> data;

}
