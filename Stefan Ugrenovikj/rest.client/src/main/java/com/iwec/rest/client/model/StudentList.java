package com.iwec.rest.client.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlType(name = "evidencija-type")
@XmlRootElement(name = "evidencija")
@XmlAccessorType(XmlAccessType.FIELD)
public class StudentList implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "data")
	@XmlElementWrapper(name = "Student")
	private List<Student> students = 
		new ArrayList<Student>();

	public List<Student> getSlusateli() {
		return students;
	}

	public void addSlusatel(Student student) {
		students.add(student);
	}
}


