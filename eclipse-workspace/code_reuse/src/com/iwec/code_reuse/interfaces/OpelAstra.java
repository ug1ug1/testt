package com.iwec.code_reuse.interfaces;

public class OpelAstra implements Car {

	@Override
	public String horn(int duration) {
		String result = "";
		for (int i = 0; i < duration; i++) {
			result += "tit/";
		}
		return result;
	}

	@Override
	public void brake() {
		System.out.println("AZZ... +");

	}

}
