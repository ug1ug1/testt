package com.iwec.myebooks.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.iwec.myebooks.model.Publisher;
import com.iwec.myebooks.service.PublisherService;

@RestController
@RequestMapping("v1/publishers")
public class PublisherController {

	@Autowired
	private PublisherService service;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Publisher> findAll() {
		return service.findAll();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Publisher> findById(
			@PathVariable(value = "id") Integer id) {

		Publisher publisher = service.findById(id);

		HttpStatus status = publisher != null
				? HttpStatus.OK
				: HttpStatus.NOT_FOUND;

		return new ResponseEntity<Publisher>(publisher, status);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Integer save(@RequestBody Publisher publisher) {
		publisher = service.save(publisher);
		return publisher != null ? publisher.getId() : null;
	}

	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public Integer update(@RequestBody Publisher publisher) {
		publisher = service.update(publisher);
		return publisher != null ? publisher.getId() : null;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable(value = "id") Integer id) {
		service.delete(id);
	}
}