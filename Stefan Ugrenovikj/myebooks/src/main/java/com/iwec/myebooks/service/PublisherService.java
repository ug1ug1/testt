package com.iwec.myebooks.service;

import com.iwec.myebooks.model.Publisher;

public interface PublisherService extends GenericService<Publisher> {

}
