package com.iwec.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileCopy {

	public void copyChars(String source, String destination) {
		BufferedReader in = null;
		FileWriter out = null;
		try {
			in = new BufferedReader(new FileReader(new File(source)));
			out = new FileWriter(new File(destination));
			String line = null;
			while ((line = in.readLine()) != null) {
				System.out.println(line);
				out.write(line + "\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Error while reading from the file specified");
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String getContent(String fileName) {
		String result = "";
		try (BufferedReader in = new BufferedReader(
				new FileReader(new File(fileName)));) {

			String line = null;
			while ((line = in.readLine()) != null) {
				result += line + '\n';
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Error while reading from the file specified");
		}
		return result;
	}

}
