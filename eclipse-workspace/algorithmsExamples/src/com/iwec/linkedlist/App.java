package com.iwec.linkedlist;

public class App {
	public static void main(String[] args) {

		LinkedList<Integer> list = new LinkedList<>();
		System.out.println(list);

		list.addAtHead(11);
		list.addAtHead(12);
		list.addAtHead(13);
		list.addAtTail(8);
		list.addAtTail(7);
		list.addAtIndex(4, 9);
		list.deleteAtIndex(2);
		
		System.out.println(list);
		System.out.println(list.size());
		
		
		System.out.println(list.get(2));
		
		System.out.println(list.getIndex(13));
		
		System.out.println(list.remove(130));
		
		System.out.println(list.remove(9));
		
		System.out.println(list);

		

		
	}
}
