package com.iwec.jdbc.dao;

import java.util.List;

public interface BookDAO<T> {
	public T findById(Integer id);
	public List<T> findAll();
	public T insert(T tk);
	public T deleteById(Integer id);
	public T update(T t);

} 