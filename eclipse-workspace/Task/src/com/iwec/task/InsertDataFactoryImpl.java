package com.iwec.task;

public class InsertDataFactoryImpl implements InsertDataFactory {

	@Override
	public String insertName() {

		return String.format("|%30s|", "Stefan");
	}

	
	public String insertLastName() {
		return String.format("|%50s|", "Ugrenovikj");
	}

	@Override
	public String insertEMail() {
		return String.format("|%50s|", "stefan@hotmail.com");
	}

	@Override
	public String insertBirthDate() {
		return String.format("|%10s|", "1997-27-03");
	}

}
