package com.iwec.junit5.sort;

import java.util.Comparator;
import java.util.List;

public class BubbleSorterLambda<T> {

	public List<T> sort(List<T> list, Comparator<T> comp) {

		int listSize = list.size();
		boolean isSorted;

		for (int i = 0; i < listSize; i++) {
			isSorted = true;
			for (int j = 1; j < (listSize - i); j++) {
				if (comp.compare(list.get(j - 1), list.get(j)) > 0) {
					swap(list, j - 1, j);
					isSorted = false;
				}
			}

			// if sorted, then break, and avoid useless loop
			if (isSorted) {
				break;
			}
		}
		return list;
	}

	private void swap(List<T> list, int x, int y) {
		T temp = list.get(x);
		list.set(x, list.get(y));
		list.set(y, temp);
	}

}
