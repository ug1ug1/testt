package com.iwec.task.readerwriter;
import com.iwec.task.model.Student;
public class ReaderStudentImpl extends ReaderStudent<Student> {

	@Override
	public Student createInstance(String arg) {
		String[] tokens = arg.split(",");
		if (tokens.length < 1) {
			System.err.println(arg + " is not valid Shape data");
			return null;
		}
		Student student = new Student(tokens[0].trim(),tokens[1].trim(), tokens[2].trim(),tokens[3].trim());
		return student;
		
	}

}
