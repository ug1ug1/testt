package com.iwec.junit5.sort;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StudentSorterTest {

	private StudentSorter sorter;

	@BeforeEach
	public void setUp() {
		// given
		sorter = new StudentSorter();
	}

	@Test
	public void testStudentSorterWhenSortsASC() {

		// given
		List<Student> students = Arrays.asList(
				new Student(1, "Круме", "Шабанов"),
				new Student(2, "Ангеле", "Ангелев"),
				new Student(3, "Ангеле", "Ангелев"),
				new Student(4, "Богољуб", "Крумев"));

		// when
		sorter.sort(students);

		// then
		assertThat(students).containsExactly(
				new Student(3, "Ангеле", "Ангелев"),
				new Student(2, "Ангеле", "Ангелев"),
				new Student(4, "Богољуб", "Крумев"),
				new Student(1, "Круме", "Шабанов"));
	}
}
