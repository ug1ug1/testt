package com.iwec.quicksort;

public class QuickSorter {

	public void quickSort(int arr[], int begin, int end) {
		if (begin < end) {
			int partitionIndex = partition(arr, begin, end);

			quickSort(arr, begin, partitionIndex - 1);
			quickSort(arr, partitionIndex + 1, end);
		}
	}

	private int partition(int arr[], int begin, int end) {
		int pivot = arr[end];
		int i = (begin - 1);

		for (int j = begin; j < end; j++) {
			if (arr[j] <= pivot) {
				i++;

				swapNumbers(arr, i, j);
			}
		}
		swapNumbers(arr, i + 1, end);
		return i + 1;

	}

	public void swapNumbers(int arr[], int i, int j) {
		int swapTemp = arr[i];
		arr[i] = arr[j];
		arr[j] = swapTemp;

	}
	
	
	
	/*
	 * public void swapNumbers2(int arr[], int i, int end) { int swapTemp = arr[i +
	 * 1]; arr[i + 1] = arr[end]; arr[end] = swapTemp;
	 * 
	 * 
	 * 
	 * }
	 */

}
