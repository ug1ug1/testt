package com.iwec.hibernate.example;

import java.util.ArrayList;
import java.util.List;

import com.iwec.hibernate.example.dao.GenericDAO;
import com.iwec.hibernate.example.dao.GenericReadOnlyDAO;
import com.iwec.hibernate.example.dao.impl.BookDAOImpl;
import com.iwec.hibernate.example.dao.impl.UserDAOImpl;
import com.iwec.hibernate.example.dao.impl.UserRoleDAOImpl;
import com.iwec.hibernate.example.model.Author;
import com.iwec.hibernate.example.model.Book;
import com.iwec.hibernate.example.model.User;
import com.iwec.hibernate.example.model.UserRole;

public class App2 {

	public static void main(String[] args) {
		GenericReadOnlyDAO<UserRole> dao = new UserRoleDAOImpl();

		System.out.println(dao.list());
		System.out.println(dao.find(1));
		
		UserRole role = dao.find(2);
		User user = new User("krume", "passwo", role);
		
		GenericDAO<User> userDao = new UserDAOImpl();
		//userDao.save(user);
		
		User usr = userDao.find(1);
		System.out.println(usr.getRole());
		
		Author author1 = new Author("Pero", "Perov");
		Author author2 = new Author("Ѓоре", "Ѓорев");
		
		List<Author> authors = new ArrayList<>();
		authors.add(author1);
		authors.add(author2);
		
		Book book = new Book("My Dreams...", authors);
		
		GenericDAO<Book> bookDao = new BookDAOImpl();
		bookDao.save(book);

		

	}

}
