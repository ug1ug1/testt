package com.iwec.shape;

public interface ShapeFacade {
	
	public void calculateArea(Integer circle, Integer triangle, Integer square);
		
	
}
