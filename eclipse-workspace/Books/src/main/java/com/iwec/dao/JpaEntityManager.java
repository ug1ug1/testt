package com.iwec.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaEntityManager {
	private static final JpaEntityManager instance = new JpaEntityManager();
	private final EntityManagerFactory entityManager;

	private JpaEntityManager() {
		entityManager = Persistence
				.createEntityManagerFactory("persistence");
	}

	public static EntityManager getEntityManager() {
		return getInstance().entityManager.createEntityManager();
	}

	private static JpaEntityManager getInstance() {
		return instance;
	}

}



