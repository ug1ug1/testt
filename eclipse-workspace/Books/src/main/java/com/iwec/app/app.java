package com.iwec.app;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.iwec.model.Author;
import com.iwec.model.Book;

public class app {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		System.out.println("Starting Transaction");
		entityManager.getTransaction().begin();
		Book book = new Book(1,"koga", "noga",1);
		
		Author author1 = new Author(1,"Tom", "Cruise", 1);
		
		System.out.println("Saving Student to Database");

		entityManager.persist(author1);
		entityManager.getTransaction().commit();
		
		System.out.println("Generated Aqwq ID = " + author1.getId());


	}

}
