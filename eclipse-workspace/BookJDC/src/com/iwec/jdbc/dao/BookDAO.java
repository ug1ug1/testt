package com.iwec.jdbc.dao;

import java.awt.print.Book;
import java.util.List;

public interface BookDAO {

	public Book findById(Integer id);

	public List<Book> findAll();

	public int insert(Book book);

	public int deleteById(Integer id);

	public int update(Book book);
}