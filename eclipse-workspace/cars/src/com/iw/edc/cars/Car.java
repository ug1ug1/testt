package com.iw.edc.cars;

public class Car {
	private String manufacturer;
	private String model;
	private int prodYear;
	private int passedKM;


	public Car(String manufacturer, String model, int prodYear) {
		this.manufacturer = manufacturer;
		this.model = model;
		this.prodYear = prodYear;
	}
	
	

	public Car(String manufacturer, String model, int prodYear, int passedKM) {
		this.manufacturer = manufacturer;
		this.model = model;
		this.prodYear = prodYear;
		this.passedKM = passedKM;
	}



	public String getManufacturer() {
		return manufacturer;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public int getProdYear() {
		return prodYear;
	}
	
	public void setProdYear(int prodYear) {
		this.prodYear = prodYear;
	}

}
