package com.iwec.deshavki;

import java.util.List;

import javax.persistence.EntityManager;

import com.iwec.dao.GenericDao;
import com.iwec.dao.JpaEntityManager;
import com.iwec.model.Author;

public class AuthorDAO implements GenericDao<Author> {

	public Author find(Integer id) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		Author result = em.find(Author.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	public List<Author> list() {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		List<Author> result = em.createQuery("from Book", Author.class)
				.getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	
	public void save(Author t) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	public void remove(Author t) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	}

}
