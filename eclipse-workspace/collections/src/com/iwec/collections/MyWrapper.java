package com.iwec.collections;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MyWrapper {

	public static boolean condition(Integer x) {

		return x <= 0;
	}

	static void myFilter(List<Integer> numbers) {
		int count = 0;
		for (Iterator<Integer> it = numbers.iterator(); it.hasNext();) {
			Integer n = it.next();
			System.out.println(count + " -> " + n);
			count++;
			if (!condition(n)) {
				// we are here only of we need to remove element
				it.remove();
			}
		}
	}
	
	public static void doPow(List<Integer> numbers) {
		for (int i = 0; i < numbers.size(); i++) {
			int x = numbers.get(i);
			numbers.set(i, x * x);
		}
	}

	public static Integer string2Int(String str) {
		Integer result = null;
		try {
			result = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			// do nothing, result remains null
		}
		return result;
	}

}
