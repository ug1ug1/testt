package com.iwec.junit5.calc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class CSVReader<T> {
	public List<T> read(String fileName) {
		List<T> result = new ArrayList<>();

		try (BufferedReader in = new BufferedReader(
				new FileReader(new File(fileName)));) {

			String line;
			while ((line = in.readLine()) != null) {
				T t = createInstance(line);
				if (t != null) {
					result.add(t);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public abstract T createInstance(String line);

}