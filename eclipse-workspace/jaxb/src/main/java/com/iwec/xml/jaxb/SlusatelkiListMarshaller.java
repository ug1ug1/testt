package com.iwec.xml.jaxb;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class SlusatelkiListMarshaller {
	private static final String XML_FILE = "slusatelki.xml";
	private SlusateliList slusatelkiList;

	public SlusatelkiListMarshaller() {
		slusatelkiList = new SlusateliList();
	}

	public void addSlusatelka(Slusatel slusatelka) {
		slusatelkiList.addSlusatel(slusatelka);
	}

	private void doMarshal() {
		try {
			JAXBContext context = JAXBContext
					.newInstance(new Class[]{SlusateliList.class});
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
					Boolean.TRUE);
			marshaller.marshal(slusatelkiList, new FileOutputStream(XML_FILE));
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		SlusatelkiListMarshaller slusatelkiListMarshaller = new SlusatelkiListMarshaller();
		slusatelkiListMarshaller.addSlusatelka(
				new Slusatel("Ѓургина", "Тапевска", "gjurka@exsample.com"));
		slusatelkiListMarshaller.addSlusatelka(
				new Slusatel("Севдие", "Бешкими", "sev_bes@exsample.com"));
		slusatelkiListMarshaller.doMarshal();
	}
}