package com.iwec.hibernate.example.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import com.iwec.hibernate.example.dao.GenericReadOnlyDAO;
import com.iwec.hibernate.example.dao.JpaEntityManagerFactory;
import com.iwec.hibernate.example.model.UserRole;

public class UserRoleDAOImpl implements GenericReadOnlyDAO<UserRole> {

	@Override
	public UserRole find(Integer id) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		UserRole result = em.find(UserRole.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public List<UserRole> list() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<UserRole> result = em.createQuery("from UserRole", UserRole.class)
				.getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

}
