package com.iwec.code_reuse.interfaces;

public interface Car {
	public String horn(int duration);
	public void brake();
}