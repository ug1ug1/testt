package com.iwec.code_reuse.interfaces;

public class Peugeot205 implements Car {

	@Override
	public String horn(int duration) {
		String result = "";
		for (int i = 0; i < duration; i++) {
			result += "biip-";
		}
		return result;
	}

	@Override
	public void brake() {
		System.out.println("Kochiiiiiiiiii...");
	}

}
