package com.iwec.shape;

public class CalculateSquare {
	
	public void calculateSquare(Integer a) {
		ShapeReader reader = new ShapeReader();
		reader.read("shapes");
		
		return a * a;
		
	}

}
