package com.iwec.linkedlist;

public class LinkedList<T> {
	private Node<T> head;
	private int count;

	public LinkedList() {
	}

	public LinkedList(T data) {
		head = new Node<T>(data);
		count++;
	}

	public void addAtHead(T data) {
		Node<T> temp = head;
		head = new Node<T>(data);
		head.setNext(temp);
		count++;
	}

	public void addAtTail(T data) {
		Node<T> temp = head;
		while (temp.getNext() != null) {
			temp = temp.getNext();
		}

		temp.setNext(new Node<T>(data));
		count++;
	}

	public void addAtIndex(int index, T data) {
		Node<T> temp = head;
		Node<T> holder;
		for (int i = 0; i < index - 1 && temp.getNext() != null; i++) {
			temp = temp.getNext();
		}
		holder = temp.getNext();
		temp.setNext(new Node<T>(data));
		temp.getNext().setNext(holder);
		count++;
	}

	public void deleteAtIndex(int index) {
		Node<T> temp = head;
		for (int i = 0; i < index - 1 && temp.getNext() != null; i++) {
			temp = temp.getNext();
		}
		temp.setNext(temp.getNext().getNext());
		count--;
	}

	public T get(int index) {
		Node<T> temp = head;
		for (int i = 0; i < index; i++) {
			temp = temp.getNext();
		}
		return temp.getData();
	}

	public int getIndex(T data) {
		Node<T> temp = head;
		int index = 0;
		while (temp != null) {
			if (temp.getData().equals(data)) {
				return index;
			}
			index++;
			temp = temp.getNext();
		}
		return -1;
	}

	public boolean remove(T data) {
		int index = getIndex(data);

		if (index == -1) {
			return false;
		}

		deleteAtIndex(index);
		return true;
	}

	public String toString() {
		String result = "[";
		Node<T> temp = head;
		while (temp != null) {
			result += temp.getData().toString() + ", ";
			temp = temp.getNext();
		}
		int lastIndexOfComma = result.lastIndexOf(",");
		if (lastIndexOfComma != -1) {
			result = result.substring(0, lastIndexOfComma);
		}
		return result + ']';
	}

	public int size() {
		return count;
	}

}