package com.iwec.xml;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XPathLocator {

	public static NodeList getNodeList(String fileName, String xPath) {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory
				.newInstance();
		domFactory.setNamespaceAware(true);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodes = null;
		try {
			Document document = domFactory.newDocumentBuilder().parse(fileName);
			XPathExpression expression = xpath.compile(xPath);
			nodes = (NodeList) expression.evaluate(document,
					XPathConstants.NODESET);
		} catch (IOException | SAXException | ParserConfigurationException
				| XPathExpressionException e) {
			System.out.println(e.getMessage());
			// e.printStackTrace();
		}
		return nodes;
	}

}