package com.iwec.graph;

public class App {

	public static void main(String[] args) {

		Graph graph = new Graph();
		
		

		graph.addVertex("Bob");
		graph.addVertex("Alice");
		graph.addVertex("Mark");
		graph.addVertex("Rob");
		graph.addVertex("Maria");
		graph.addEdge("Bob", "Alice");
		graph.addEdge("Bob", "Rob");
		graph.addEdge("Alice", "Mark");
		graph.addEdge("Rob", "Mark");
		graph.addEdge("Alice", "Maria");
		graph.addEdge("Rob", "Maria");

		System.out.println(graph);
		System.out.println(graph.directEdgeTest(graph, "Mark"));

		// [Bob, Rob, Maria, Alice, Mark]
		System.out.println(
				GraphTraversal.depthFirstTraversal(graph, "Bob").toString());

		// [Bob, Alice, Rob, Mark, Maria]
		System.out.println(
				GraphTraversal.breadthFirstTraversal(graph, "Bob").toString());

	}

}
