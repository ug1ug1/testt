package com.iwec.myebooks.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iwec.myebooks.dao.PublisherRepository;
import com.iwec.myebooks.model.Publisher;
import com.iwec.myebooks.service.PublisherService;

@Service
public class PublisherServiceImpl implements PublisherService {

	@Autowired
	private PublisherRepository repository;

	@Override
	public List<Publisher> findAll() {
		return (List<Publisher>) repository.findAll();
	}

	@Override
	public Publisher findById(Integer id) {
		if (id == null) {
			return null;
		}

		Optional<Publisher> publisher = repository.findById(id);
		return publisher.orElse(null);
	}

	@Override
	public Publisher save(Publisher publisher) {
		if (publisher == null) {
			return null;
		}
		publisher = repository.save(publisher);
		return publisher;
	}

	@Override
	public Publisher update(Publisher publisher) {
		// TODO read from database, update instance, save to database
		return save(publisher);
	}

	@Override
	public void delete(Integer id) {
		repository.deleteById(id);
	}

}
