package com.iwec.binarytree;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {
	private Node root;

	public Node getRoot() {
		return root;
	}

	public void add(int value) {
		root = addRecursive(root, value);
	}

	private Node addRecursive(Node current, int value) {
		if (current == null) {
			return new Node(value);
		}

		if (value < current.getValue()) {
			current.setLeft(addRecursive(current.getLeft(), value));
		} else if (value > current.getValue()) {
			current.setRight(addRecursive(current.getRight(), value));
		} else {
			return current;
		}

		return current;
	}

	public void traverseInOrder(Node node) {
		if (node != null) {
			traverseInOrder(node.getLeft());
			System.out.print(" " + node.getValue());
			traverseInOrder(node.getRight());
		}
	}

	public void traversePreOrder(Node node) {
		if (node != null) {
			System.out.print(" " + node.getValue());
			traversePreOrder(node.getLeft());
			traversePreOrder(node.getRight());
		}
	}

	public void traversePostOrder(Node node) {
		if (node != null) {
			traversePostOrder(node.getLeft());
			traversePostOrder(node.getRight());
			System.out.print(" " + node.getValue());
		}
	}

	public void traverseLevelOrder() {
		if (root == null) {
			return;
		}

		Queue<Node> nodes = new LinkedList<>();
		nodes.add(root);

		while (!nodes.isEmpty()) {
			Node node = nodes.remove();

			System.out.print(" " + node.getValue());

			if (node.getLeft() != null) {
				nodes.add(node.getLeft());
			}

			if (node.getRight() != null) {
				nodes.add(node.getRight());
			}
		}
	}

	public boolean contains(int value) {
		return containsRecursive(root, value);
	}

	private boolean containsRecursive(Node current, int value) {
		if (current == null) {
			return false;
		}

		if (value == current.getValue()) {
			return true;
		}

		return value < current.getValue()
				? containsRecursive(current.getLeft(), value)
				: containsRecursive(current.getRight(), value);
	}

	public void delete(int value) {
		root = deleteRecursive(root, value);
	}

	private Node deleteRecursive(Node current, int value) {
		if (current == null) {
			return null;
		}

		if (value == current.getValue()) {
			// case 1: no children
			if (current.getLeft() == null && current.getRight() == null) {
				return null;
			}

			// case 2: only 1 child
			if (current.getRight() == null) {
				return current.getLeft();
			}

			if (current.getLeft() == null) {
				return current.getRight();
			}

			// case 3: 2 children
			int smallestValue = findSmallestValue(current.getRight());
			current.setValue(smallestValue);
			current.setRight(
					deleteRecursive(current.getRight(), smallestValue));
			return current;
		}

		if (value < current.getValue()) {
			current.setLeft(deleteRecursive(current.getLeft(), value));
			return current;
		}

		current.setRight(deleteRecursive(current.getRight(), value));
		return current;
	}

	private int findSmallestValue(Node root) {
		return root.getRight() == null
				? root.getValue()
				: findSmallestValue(root.getLeft());
	}

	public boolean isEmpty() {
		return root == null;
	}

	public int getSize() {
		return getSizeRecursive(root);
	}

	private int getSizeRecursive(Node current) {
		return current == null
				? 0
				: getSizeRecursive(current.getLeft()) + 1
						+ getSizeRecursive(current.getRight());
	}

}
