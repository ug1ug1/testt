package com.iwec.code_reuse.interfaces;

public class App {

	public static void main(String[] args) {
		Car peugeot = new Peugeot205();
		//Car opel = new OpelAstra();
		
		Car[] cars = new Car[2];
		
		cars[0] = peugeot;
		cars[1] = new OpelAstra();
		
		for (Car car : cars) {
			car.brake();
			System.out.println(car.horn(5));
		}
		
		/*
		 * peugeot.brake(); opel.brake();
		 * 
		 * System.out.println();
		 * 
		 * System.out.println(peugeot.horn(3));
		 * System.out.println(opel.horn(5));
		 */
	}

}
