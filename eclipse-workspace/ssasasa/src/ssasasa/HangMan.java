package ssasasa;


/*
 * #part of gamePak project
 * 
 *  
 
 *  
 * HangMan.java
 * written by Thinh Ngo at South Seattle Community College
 * in part of programming class CSC 142 on February 10,2009
 */
import java.util.Scanner;


public class HangMan {
	
	//a list of words to be use
	static String[] wordList = {"abstraction",
    "ambiguous",
    "arithmetic",
    "backslash",
    "bitmap",
    "circumstance",
    "combination",
    "consequently",
    "consortium",
    "decrementing",
    "dependency",
    "disambiguate",
    "dynamic",
    "encapsulation",
    "equivalent",
    "expression",
    "facilitate",
    "fragment",
    "hexadecimal",
    "implementation",
    "indistinguishable",
    "inheritance",
    "internet",
    "java",
    "localization",
    "microprocessor",
    "navigation",
    "optimization",
    "parameter",
    "patrick",
    "pickle",
    "polymorphic",
    "rigorously",
    "simultaneously",
    "specification",
    "structure",
    "lexical",
    "likewise",
    "management",
    "manipulate",
    "mathematics",
    "hotjava",
    "vertex",
    "unsigned",
    "primitives",
    "traditional"}; 

	
	
	
	static int randomThings; //variable that will decide which and what word would be use
	static String word;  // current word in use
	static int count=0;  // count mistakes, and 6 would end the game
	public static void main(String[] args){ // beginning of the game

		boolean endGame=false; // init telling the game is not intended to end yet 
		char now;  // declaring a char variable used for deciding which letter is being evaluated
		String response; // declaring a variable use to taking user's input
		char letter;  // take the first letter of response and make it the user input
		String letterUsed; // a list of user's input that is wrong
		char[] wordChar; // a blank sheet, if the letter is correct, the respected array will be filled in
		Scanner scan = new Scanner(System.in); //init scanner


		do{  // run the program until while is satisfied
			
			wordChar=randomPickWord();  //pick a word
			letterUsed=""; //clear the incorrect letter list
			response = " "; // clear the response
			
		while(word.equals(String.valueOf(wordChar))==false && (response.toLowerCase().equals("exit")==false & count!=6)){			

			drawMan(count); //draw the dancing man
			System.out.println(); // create a blank line
			for(int i=0;i<word.length();i++){ // checking to see if the letter is filled, also if it is not then create _
				now=word.charAt(i);				
				if(now!=32 && wordChar[i]==32){
					System.out.print("_");
				}else if(now!=32 && wordChar[i]!=32){
					System.out.print(wordChar[i]);
				}else{
					System.out.print(" ");
				}
			}

			System.out.println("\nPick a letter: \t\t"+letterUsed); //show the letter that is used already and ask for a new letter
			response=scan.next(); //ask for a letter
			letter=response.toLowerCase().charAt(0); //take the first letter of the response
			
			if(word.replace(letter, (char)48)==word && letterUsed.replace(letter, (char)48)==letterUsed){ //check to see if letter is already declard, if not add to a list
				count=count+1; //this is one strike against the user
				letterUsed=letter+ " "+letterUsed;
			}else if(word.replace(letter, (char)48)==word && letterUsed.replace(letter, (char)48)!=letterUsed){
				System.out.println("Letter may already be used! ");
			}
			
			for(int i=0;i<word.length();i++){ // if the letter is correct, fill in a list
				if(word.toLowerCase().charAt(i)==letter)wordChar[i]=word.charAt(i);


				if(response.equals("exit")==true)System.exit(0); //end the program if the user get sick of it

			}
		}
		
		if(response.toLowerCase().equals("exit")==false && word.equals(String.valueOf(wordChar))==false){ //the user has lost, show man dead and tell user have option to play again
		drawMan(6);
		System.out.println("You are dead, press exit to stop game!  Or 1 and enter to keep playing.");
		count=0;
		}else if(word.equals(String.valueOf(wordChar))==true){
			
			System.out.println("* * @  #   * \n *  * # \n $  & # % \n YOU WINS!");
			
		}
		
		System.out.println("winning word is: "+word); //let the user see the winning word
		
		response=scan.next(); 
		
		if(response.equals("exit"))endGame=true; // end game if user say exit now
		
		}while(endGame==false);

	}







	private static char[] randomPickWord() { // pick a random word

		randomThings=(int)((Math.random()*(wordList.length))+1);//create random variable
		int lengthWord=wordList[randomThings-1].length(); //pick a word
		char[] wordChar=new char[lengthWord]; // create a blank word as big as the random word selected 

		for(int i=0;i<lengthWord;i++){ // blanking them out with spaces just to make sure:)
			wordChar[i]=32;
		}
		word=wordList[randomThings-1]; //the random word chosen



		return wordChar; // give out the random word with the corresponding amount of spaces back to the main argument
	}

	private static void drawMan(int x){  // drawing the man

		System.out.print("________"); 
		if(x>6)
			System.out.print("\n|      |");
		System.out.print("\n|");

		if(x>0)
			System.out.print("      0");
		System.out.print("\n|");
		if(x>1)
			System.out.print("     \\|/");
		System.out.print("\n|");
		if(x>2)
			System.out.print("      |");
		if(x<6)
			System.out.print("\n|");
		if(x>3 && x<6)
			System.out.print("     /");
		if(x>4 && x<6)
			System.out.print(" \\");
		if(x<6)
			System.out.print("\n==============");

		if(x>5){
			System.out.print("\n===== / \\ \\===");
			System.out.print("\ndead!      \\  ");
		}




	}


}
//end of program