package com.iwec.geometry;

import java.util.Random;

public class Point {

	private char mark;
	private int x;
	private int y;
	private Random rnd;
	private final static int BOUND = 10;

	public Point() {
		mark = 'A';
		rnd = new Random();
		x = rnd.nextInt(BOUND);
		y = rnd.nextInt(BOUND);

	}

	public Point(char mark, int x, int y) {

		this.mark = mark;
		rnd = new Random();
		x = rnd.nextInt(BOUND);
		y = rnd.nextInt(BOUND);

	}

	public char getMark() {
		return mark;
	}

	public void setMark(char mark) {
		this.mark = mark;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String print() {
		return String.format("%s(%d, %d)", mark, x, y);

	}

}
