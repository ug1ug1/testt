package com.iwec.junit5.calc;

public interface Calculator {
	
	public int add(int x, int y) throws RuntimeException;

}
