package com.iwec.code_reuse.shape;

public class App {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[4];
		
		shapes[0] = new Circle(50);
		shapes[1] = new Rectangle(5, 4);
		shapes[2] = new Rectangle(22, 33);
		shapes[3] = new Circle(15);
		
		double totalArea = 0.0d;
		for (Shape shape : shapes) {
			totalArea += shape.calculateArea();
			
			System.out.println(shape);
		}
		
		System.out.println(totalArea);
	}

}
