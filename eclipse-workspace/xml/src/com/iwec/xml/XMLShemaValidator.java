package com.iwec.xml;

import java.io.IOException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

public class XMLShemaValidator {

	public static boolean test(String xmlFile, String xmlShema) {
		boolean valid = true;
		SchemaFactory factory = SchemaFactory
				.newInstance("http://www.w3.org/2001/XMLSchema");
		try {
			Schema schema = factory.newSchema(new StreamSource(xmlShema));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(xmlFile));
		} catch (IOException | SAXException e) {
			valid = false;
			//e.printStackTrace();
			System.err.println(e.getMessage());
		}
		return valid;
	}

}