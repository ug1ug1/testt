package com.iwec.rest.client;

import java.util.Map;

import com.jayway.jsonpath.JsonPath;

import net.minidev.json.JSONArray;

public class App2 {

	public static void main(String[] args) {
		
		HTTPClient httpClient = new HTTPClient();
		
		String json = httpClient.readFileAsString("/students.json");

		Map<String, String> map = httpClient.getJSONAsMap(json);
		System.out.println(map);

		JSONArray jsonArray = JsonPath.read(json, "$.data[*].['first_name', 'last_name', 'email']");

		for (Object object : jsonArray) {
			System.out.println(object);
			
		}

	}

}
