package com.edu.iw.client;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.edu.iw.model.*;

public class Client {
	
	public Model fetchStudent(Integer id) {
		try {
			HttpClient client = HttpClients.createDefault();
			HttpGet request = new HttpGet("http://localhost:8080/rest/students/" + id);
			request.setHeader("Accept", "application/json");
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return mapper.readValue(entity.getContent(), Student.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
