package daa;

public class Recursion {
	private int count;
	public int getCount() {
		return count;
	}

	public void countWord(String str, String c) {
		if (str.contains(c)) {
			count++;
			str = str.substring(str.indexOf(c) + c.length());
			 countWord(str, c);
		}
	
	}
}