package com.iwec.snaps.sorter;

import java.util.List;

import com.iwec.snaps.model.StudentDTO;

public class BubbleSorter {

	public List<StudentDTO> bubbleSort(List<StudentDTO> students) {
		int i = 0;
		boolean swapNeeded = true;
		while (i < students.size() - 1 && swapNeeded) {
			swapNeeded = false;
			for (int j = 1; j < students.size() - i; j++) {
				int x = students.get(j - 1).getLastName()
						.compareTo(students.get(j).getLastName());
				// TODO check what if x == 0
				// TODO separate private swap method
				// TODO work with generics
				if (x > 0) {
					StudentDTO temp = students.get(j - 1);
					students.set(j - 1, students.get(j));
					students.set(j, temp);
					swapNeeded = true;
				}
			}
			if (!swapNeeded) {
				break;
			}
			i++;
		}
		
		return students;
	}

}
