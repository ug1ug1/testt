package com.iwec.xml.jaxb;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class SlusateliListUnmarshaller {

	private static final String XML_FILE = "/slusatelki.xml";
	private SlusateliList slusateliList;

	public SlusateliListUnmarshaller() {
		doUnmarshal();
	}

	public SlusateliList getSlusateliList() {
		return slusateliList;
	}

	private void doUnmarshal() {
		try {
			JAXBContext context = JAXBContext
					.newInstance(new Class[]{SlusateliList.class});
			Unmarshaller unmarshaller = context.createUnmarshaller();
			InputStream in = this.getClass().getResourceAsStream(XML_FILE);
			slusateliList = (SlusateliList) unmarshaller.unmarshal(in);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SlusateliListUnmarshaller xml = new SlusateliListUnmarshaller();
		System.out.println(xml.getSlusateliList().getSlusateli());
	}
}
