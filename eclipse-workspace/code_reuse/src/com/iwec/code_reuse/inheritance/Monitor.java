package com.iwec.code_reuse.inheritance;

public abstract class Monitor {
	protected boolean on;
	private int contrast;

	public Monitor(boolean on, int contrast) {
		super();
		this.on = on;
		this.contrast = contrast;
	}

	public boolean isOn() {
		return on;
	}

	public void setOn(boolean on) {
		this.on = on;
	}
	
	public int getContrast() {
		return contrast;
	}
	
	public void setContrast(int contrast) {
		this.contrast = contrast;
	}
	
	public abstract void doSomething();

}
