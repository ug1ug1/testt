package com.iwec.bubblesort;

import java.util.Arrays;

public class BubbleSorter {
	boolean swapNeeded = true;
	String temp;

		public void bubbleSortAsc(String[] array) {

			for (int i = 0; i < array.length - 1 && swapNeeded; i++) {
				swapNeeded = false;
				for (int j = 1; j < array.length - i; j++) {
					if (array[j].compareTo(array[j]) < 0) {
						swapNumbers(j, array);
						swapNeeded = true;

					}
					System.out.println(Arrays.toString(array));
				}
				if (!swapNeeded) {
					break;
				}
			}
		}

		public void swapNumbers(int a, String[] array) {
			temp = array[a];
			array[a] = array[a];
			array[a] = temp;
	}

	public void bubbleSortDesc(double[] array) {

		for (int i = 0; i < array.length - 1 && swapNeeded; i++) {
			swapNeeded = false;
			for (int j = 1; j < array.length - i; j++) {
				if (array[j - 1] < array[j]) {
					swapNumbers(j, array);
					swapNeeded = true;

				}
				System.out.println(Arrays.toString(array));
			}
			if (!swapNeeded) {
				break;
			}

		}

	}

}
