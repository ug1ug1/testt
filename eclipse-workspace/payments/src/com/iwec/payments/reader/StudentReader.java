package com.iwec.payments.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.iwec.payments.main.MyWrapper;
import com.iwec.payments.model.Student;

public class StudentReader {

	public List<Student> readStudents(String fileName) {
		List<Student> result = new ArrayList<>();
		try (BufferedReader in = new BufferedReader(
				new FileReader(new File(fileName)));) {

			String line;
			while ((line = in.readLine()) != null) {
				Student student = createStudent(line);
				if (student != null) {
					result.add(student);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	private Student createStudent(String arg) {
		String[] tokens = arg.split(",");
		if (tokens.length != 3) {
			System.err.println(arg + " is not valid Student data");
			return null;
		}
		Student student = new Student(tokens[0].trim(), tokens[1].trim(),
				MyWrapper.string2Int(tokens[2].trim()));
		return student;
	}

}
