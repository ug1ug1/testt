package carReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Car.iwedu.Car;


public class Reader {

	public List<Car> readCar(String fileName) {
		List<Car> result = new ArrayList<>();
		try (BufferedReader in = new BufferedReader(new FileReader(new File(fileName)));) {

			String line;
			while ((line = in.readLine()) != null) {
				Car rc = createCar(line);
				if (rc != null) {
					result.add(rc);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	private Car createCar(String arg)  {
		String[] tokens = arg.split(",");
		if (tokens.length != 2) {
			System.err.println(arg + " is not valid Car data");
			return null;
		}
		 Car rc = new Car(tokens[0].trim(), tokens[1].trim(), 0);
		return rc;
		
	}
}
