package com.iwec.files;

public class App {
	public final static String SRC = "mhm.html";
	public final static String DEST = "kopija.txt";
	
	public static void main(String[] args) {
		FileCopy fc = new FileCopy();
		fc.copyChars(SRC, DEST);

		String content = fc.getContent(SRC);
		System.out.println(content);
	}

}
