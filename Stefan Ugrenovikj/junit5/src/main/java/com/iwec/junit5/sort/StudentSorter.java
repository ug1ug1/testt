package com.iwec.junit5.sort;

public class StudentSorter extends BubbleSorter<Student> {

	@Override
	public int compare(Student st1, Student st2) {
		int byLastName = st1.getLastName().compareTo(st2.getLastName());

		return (byLastName != 0)
				? byLastName
				: st1.getFirstName().compareTo(st2.getFirstName());
	}

}
