package com.iwec.deshavki;

import java.util.List;

import javax.persistence.EntityManager;

import com.iwec.dao.GenericDao;
import com.iwec.dao.JpaEntityManager;
import com.iwec.model.Category;

public class CategoryDAO implements GenericDao<Category> {

	public Category find(Integer id) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		Category result = em.find(Category.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	public List<Category> list() {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		List<Category> result = em.createQuery("from Book", Category.class)
				.getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	
	public void save(Category t) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	public void remove(Category t) {
		EntityManager em = JpaEntityManager.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	}

}
