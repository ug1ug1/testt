package com.iwec.snaps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwec.snaps.dao.StudentRepository;
import com.iwec.snaps.http.HTTPClient;
import com.iwec.snaps.model.StudentDTO;
import com.iwec.snaps.sorter.BubbleSorter;
import com.iwec.snaps.writer.FileWriter;

import net.minidev.json.JSONArray;

public class SnapFacade {

	private static final String URL = "https://reqres.in/api/users?page=";
	private static final String JSON_FILE_NAME = "students.json";
	private static final String CSV_FILE_NAME = "students.csv";

	@Autowired
	private StudentRepository repository;

	public void doSnaps() {

		// snap 1
		JSONArray jsonArray = doSnap1(1, 4);

		System.out.println(jsonArray);

		// snap 2
		doSnap2(jsonArray);

		// snap 3
		List<StudentDTO> studentList = doSnap3(jsonArray);

		System.out.println(studentList);

		// snap 4
		studentList = doSnap4(studentList);

		System.out.println(studentList);

		// snap 5
		doSnap5(studentList);

		// snap 6
		doSnap6(studentList);

	}

	/**
	 * Read JSON data as array, from exposed API
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	private JSONArray doSnap1(int from, int to) {
		JSONArray result = new JSONArray();

		for (int i = from; i <= to; i++) {
			result.addAll(new HTTPClient().getStudentsAsJSON(URL + i));
		}

		return result;
	}

	/**
	 * Save json content to file.
	 * 
	 * @param jsonArray
	 */
	private void doSnap2(JSONArray jsonArray) {
		FileWriter.write(JSON_FILE_NAME, jsonArray.toJSONString());
	}

	/**
	 * Transform JSONArray into List of custom objects
	 * 
	 * @param jsonArray
	 * @return
	 */
	private List<StudentDTO> doSnap3(JSONArray jsonArray) {
		ObjectMapper mapper = new ObjectMapper();
		List<StudentDTO> result = new ArrayList<>(jsonArray.size());

		try {
			result = mapper.readValue(jsonArray.toJSONString(),
					new TypeReference<List<StudentDTO>>() {
					});
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Sorts the list of students.
	 * 
	 * @param jsonArray
	 * @return
	 */
	private List<StudentDTO> doSnap4(List<StudentDTO> students) {
		return new BubbleSorter().bubbleSort(students);
	}

	/**
	 * Saves data in CSV format.
	 * 
	 * @param students
	 */
	private void doSnap5(List<StudentDTO> students) {
		String result = students.stream().map(x -> x.toString())
				.collect(Collectors.joining("\n"));

		FileWriter.write(CSV_FILE_NAME, result);
	}

	/**
	 * Save data into the database
	 * @param students
	 */
	private void doSnap6(List<StudentDTO> students) {
		
		for (StudentDTO student : students) {
			repository.save(student);
		}
		
	}

}
