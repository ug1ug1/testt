package com.iwec.map;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MyMap<K, V> {
	private int size;
	private static final int DEFAULT_CAPACITY = 16;

	@SuppressWarnings("unchecked")
	private Entry<K, V>[] entries = new Entry[DEFAULT_CAPACITY];

	public V get(K key) {
		for (int i = 0; i < size; i++) {
			if (entries[i] != null) {
				if (entries[i].getKey().equals(key)) {
					return entries[i].getValue();
				}
			}
		}
		return null;
	}

	public void put(K key, V value) {
		int x = 0;
		for (int i = 0; i < size; i++) {
			if (entries[i].getKey().equals(key)) {
				entries[i].setValue(value);
				x = 0;
			}
		}
		if (x != 0) {
			ensureCapacity();
			entries[size++] = new Entry<K, V>(key, value);
		}
	}

	private void ensureCapacity() {
		if (size == entries.length) {
			int newSize = entries.length * 2;
			entries = Arrays.copyOf(entries, newSize);
		}
	}

	public int size() {
		return size;
	}

	public void remove(K key) {
		for (int i = 0; i < size; i++) {
			if (entries[i].getKey().equals(key)) {
				entries[i] = null;
				size--;
				condenseArray(i);
			}
		}
	}

	private void condenseArray(int start) {
		for (int i = start; i < size; i++) {
			entries[i] = entries[i + 1];
		}
	}

	public Set<K> keySet() {
		Set<K> set = new HashSet<K>();
		for (int i = 0; i < size; i++) {
			set.add(entries[i].getKey());
		}
		return set;
	}

	public String toString() {
		String result = "{";
		Set<K> keys = keySet();
		for (K key : keys) {
			result += "[" + key + " => " + get(key) + "], ";
		}

		int lastIndexOfComma = result.lastIndexOf(",");
		if (lastIndexOfComma != -1) {
			result = result.substring(0, lastIndexOfComma);
		}

		return result + "}";
	}
}