package com.iwec.files_copier;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopier {
	public static boolean copyBytes(String original, String destination) {
		boolean ok = true;
		try (FileInputStream in = new FileInputStream(original);
				FileOutputStream out = new FileOutputStream(destination);) {

			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}

		} catch (FileNotFoundException e) {
			ok = false;
			e.printStackTrace();
		} catch (IOException e) {
			ok = false;
			e.printStackTrace();
		}
		return ok;
	}
}