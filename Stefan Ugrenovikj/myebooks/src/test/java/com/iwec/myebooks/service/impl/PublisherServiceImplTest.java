package com.iwec.myebooks.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.iwec.myebooks.dao.PublisherRepository;
import com.iwec.myebooks.model.Publisher;

@ExtendWith(MockitoExtension.class)
public class PublisherServiceImplTest {

	@Mock
	private PublisherRepository repository;

	@InjectMocks
	private PublisherServiceImpl service;

	@Test
	public void testFindByNull() {
		Publisher result = service.findById(null);
		assertEquals(null, result);
	}

	@Test
	public void testFindByIdWhenFound() {
		Publisher publisher = new Publisher(10, "Appress");

		when(repository.findById(10))
				.thenReturn(Optional.of(new Publisher(10, "Appress")));

		assertEquals(publisher, service.findById(10));
	}
	@Test
	public void testFindByIdWhenNotFound() {
		Publisher publisher = new Publisher(null, null);
		when(repository.findById(10))
				.thenReturn(Optional.of(new Publisher(null, null)));

		assertEquals(publisher, service.findById(10));
	
	}

}