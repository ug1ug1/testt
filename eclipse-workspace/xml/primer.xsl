<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
  <title>Stefan's Big Company</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
  <body>
  <h2>Lihva:</h2>
    <table border="1">
      <tr bgcolor="#ffff00">
        <th align="left">Name</th>
        <th align="left">Lastname</th>
		<th align="left">id</th>
		<th align="left">Amount (.eur)</th>
                <th align ="left">Attention</th>
      </tr>
      <xsl:for-each select="students/student[amount>5000]">
      <tr>
        <td><xsl:value-of select="name"/></td>
        <td><xsl:value-of select="lastname"/></td>
		<td><xsl:value-of select="@id"/></td>
		<td><xsl:value-of select="amount"/></td>
                 <td><xsl:value-of select="attention"/></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>
