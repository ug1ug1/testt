package com.iwec.rest.marshaller;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.iwec.rest.client.model.StudentList;



public class Unmarshaller {
	
	private static final String JSON_FILE = "/students.json";
	
	private StudentList student;
	
	public Unmarshaller() {
		doUnmarshal();
	}

	public StudentList getSlusateliList() {
		return student;
	}
	private void doUnmarshal() {
		try {
			JAXBContext context = JAXBContext
					.newInstance(new Class[]{StudentList.class});
			Unmarshaller unmarshaller = context.createUnmarshaller();
			InputStream in = this.getClass().getResourceAsStream(JSON_FILE);
			student = (StudentList) unmarshaller.unmarshal(in);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	
}
