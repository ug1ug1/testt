package com.iwec.endpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.iwec.books.model.BookDetailsRequest;
import com.iwec.books.model.BookDetailsResponse;
import com.iwec.repository.BooksRepository;

@Endpoint
public class BookEndPoint {
	private static final String NAMESPACE_URI = "model.books.iwec.com";
	@Autowired
	private BooksRepository bookRepository;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "BookDetailsRequest")
	@ResponsePayload
	public BookDetailsResponse getBook(
			@RequestPayload BookDetailsRequest request) {
		BookDetailsResponse response = new BookDetailsResponse();
		/*Book book = bookRepository.findBook(request.getId());
		System.err.println("##########" + book);
		response.setBook(book);*/
	   response.setBook(bookRepository.findBook(request.getId()));

		return response;
	}


	/*@PayloadRoot(namespace = NAMESPACE_URI, localPart = "InsertBookRequest")
	@ResponsePayload
	public InsertBookResponse inBook(
			@RequestPayload InsertBookRequest request) {
		InsertBookResponse response = new InsertBookResponse();
		response.setBook(bookRepository.insertBook(request.getId(), request.getTitle(),request.getSubtitle(),request.getIsbn()));

		return response;*/

}
