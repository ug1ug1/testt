package com.iwec.soap.endpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.iwec.soap.model.StudentDetailsRequest;
import com.iwec.soap.model.StudentDetailsResponse;
import com.iwec.soap.repository.StudentRepository;

@Endpoint
public class StudentEndpoint {
	private static final String NAMESPACE_URI = "model.soap.iwec.com";

	@Autowired
	private StudentRepository studentRepository;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "StudentDetailsRequest")
	@ResponsePayload
	public StudentDetailsResponse getStudent(
			@RequestPayload StudentDetailsRequest request) {
		StudentDetailsResponse response = new StudentDetailsResponse();
		response.setStudent(studentRepository.findStudent(request.getId()));

		return response;
	}
}