package com.iwec.snaps.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iwec.snaps.model.StudentDTO;

public interface StudentRepository extends JpaRepository<StudentDTO, Integer> {

}