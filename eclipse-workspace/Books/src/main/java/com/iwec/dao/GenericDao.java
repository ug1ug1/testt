package com.iwec.dao;

public interface GenericDao<T> extends GenericReadDao <T> {
	public void save(T t);
	public void remove(T t);

}
