package com.iwec.junit5.sort;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BubbleSorterLambdaTest {

	private BubbleSorterLambda<Student> sorter;

	@BeforeEach
	public void setUp() {
		// given
		sorter = new BubbleSorterLambda<Student>();
	}

	@Test
	public void testBubbleSorterLambdaWhenSortsASC() {

		// given
		List<Student> students = Arrays.asList(
				new Student(1, "Круме", "Шабанов"),
				new Student(2, "Шабан", "Ангелев"),
				new Student(3, "Ангеле", "Ангелев"),
				new Student(4, "Богољуб", "Крумев"));

		// when
		// TODO use method reference instead lambda expression
		sorter.sort(students, (st1, st2) -> {
			int byLastName = st1.getLastName().compareTo(st2.getLastName());

			return (byLastName != 0)
					? byLastName
					: st1.getFirstName().compareTo(st2.getFirstName());
		});

		// then
		assertThat(students).containsExactly(
				new Student(3, "Ангеле", "Ангелев"),
				new Student(2, "Шабан", "Ангелев"),
				new Student(4, "Богољуб", "Крумев"),
				new Student(1, "Круме", "Шабанов"));
	}
}
