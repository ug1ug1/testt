package io.swagger.api;

import io.swagger.model.Author;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-04-12T13:42:15.786Z")

@Controller
public class AuthorApiController implements AuthorApi {

    private static final Logger log = LoggerFactory.getLogger(AuthorApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public AuthorApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> addauthor(@ApiParam(value = "author that needs to be added to the library" ,required=true )  @Valid @RequestBody Author body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deleteauthor(@ApiParam(value = "The id of the author that needs to be deleted.",required=true) @PathVariable("id") Integer id) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Author> getauthorById(@ApiParam(value = "The id of the author that needs to be fetched. Use 1 for testing. ",required=true) @PathVariable("id") Integer id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json; charset=utf-8")) {
            try {
                return new ResponseEntity<Author>(objectMapper.readValue("{  \"lastName\" : \"lastName\",  \"name\" : \"name\"}", Author.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json; charset=utf-8", e);
                return new ResponseEntity<Author>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Author>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Author> listAllauth() {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json; charset=utf-8")) {
            try {
                return new ResponseEntity<Author>(objectMapper.readValue("{  \"lastName\" : \"lastName\",  \"name\" : \"name\"}", Author.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json; charset=utf-8", e);
                return new ResponseEntity<Author>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Author>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateauthor(@ApiParam(value = "",required=true) @PathVariable("id") Integer id,@ApiParam(value = "Updated author object" ,required=true )  @Valid @RequestBody Author body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
