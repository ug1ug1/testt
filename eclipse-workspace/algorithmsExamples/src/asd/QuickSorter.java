package asd;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QuickSorter {

	private Function<Integer, Predicate<Integer>> smallerThan;

	// TODO make constructor parameter to be Predicate
	public QuickSorter(Function<Integer, Predicate<Integer>> smallerThan) {
		this.smallerThan = smallerThan;
	}
	
	public List<Integer> quickSort(List<Integer> l) {
		if (l.isEmpty())
			return new ArrayList<>();

		return Stream
				.concat(Stream.concat(
						quickSort(l.stream().skip(1)
								.filter(smallerThan.apply(l.get(0)))
								.collect(Collectors.toList())).stream(),
						Stream.of(l.get(0))),
						quickSort(l.stream().skip(1)
								.filter(smallerThan.apply(l.get(0)).negate())
								.collect(Collectors.toList())).stream())
				.collect(Collectors.toList());
	}

}