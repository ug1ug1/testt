package com.iwec.payments.main;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.iwec.payments.model.Student;
import com.iwec.payments.reader.StudentReader;

public class App {

	public static void main(String[] args) {
		String fileName = "students.csv";
		StudentReader reader = new StudentReader();
		List<Student> students = reader.readStudents(fileName);
		// System.out.println(students);

		Set<Student> studentSet = new HashSet<>();
		studentSet.addAll(students);
		System.out.println(studentSet);

		// removeIf
		// students.removeIf(e -> e.getPayment() > 3000);
		// System.out.println(students);

		// create an instance of Stream using Collection, invoke filter with
		// Predicate and then collect the result with Collectors
		Set<Student> filteredStudents = studentSet.stream()
				.filter(e -> e.getPayment() <= 3000)
				.collect(Collectors.toSet());
		

		System.out.println(filteredStudents);
		
		students.sort((s1,s2) -> Integer.compare(s2.getPayment(), s1.getPayment()));
}

}
