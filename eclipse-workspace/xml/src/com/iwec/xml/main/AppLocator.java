package com.iwec.xml.main;

import org.w3c.dom.NodeList;

import com.iwec.xml.XPathLocator;

public class AppLocator {
	public static void main(String[] args) {
		NodeList nodes = XPathLocator.getNodeList("Untitled.xml", "//student/attention/text()");
		
		for (int i = 0; i < nodes.getLength(); i++) {
			System.out.println(nodes.item(i).getNodeValue());
		}
	}
}
