package com.iwec.dao;

import java.util.List;

public interface GenericReadDao<T> {
	public T find(Integer id);
	public List<T> list();

}
