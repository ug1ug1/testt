package com.iwec.xml.jaxb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "evidencija-type")
@XmlRootElement(name = "evidencija")
@XmlAccessorType(XmlAccessType.FIELD)
public class SlusateliList implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "slusatel")
	@XmlElementWrapper(name = "slusateli")
	private List<Slusatel> slusateli = 
		new ArrayList<Slusatel>();

	public List<Slusatel> getSlusateli() {
		return slusateli;
	}

	public void addSlusatel(Slusatel slusatel) {
		slusateli.add(slusatel);
	}
}
