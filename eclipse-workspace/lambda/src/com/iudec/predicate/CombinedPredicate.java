package com.iudec.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class CombinedPredicate {

	private static final List<String> languages = Arrays.asList("Java", "Scala", "C++", "Delphy", "Pascal");

	private Predicate<String> endsWithA = n -> n.endsWith("a");
	private Predicate<String> fourOrMoreLettersLong = n -> n.length() >= 4;

	public void filter() {
		languages.stream().filter(endsWithA.and(fourOrMoreLettersLong))
				.forEach((n) -> System.out.print("\nEnds with 'a' and is four or more letters long: " + n));

	}

	public static void main(String[] args) {
		new CombinedPredicate().filter();
	}

}
