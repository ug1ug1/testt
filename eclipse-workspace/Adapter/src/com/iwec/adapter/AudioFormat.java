package com.iwec.adapter;

public class AudioFormat implements AudioPlayer {

	private AdvancedAudioPlayer advancedAudioPlayer;

	public AudioFormat(String audioType) {
		if ("mp3".equalsIgnoreCase(audioType)) {
			advancedAudioPlayer = new MP3Player();
		} else if ("ogg".equalsIgnoreCase(audioType))
			advancedAudioPlayer = new OGG();
	}

	public void play(String filename) {

		advancedAudioPlayer.playFile(filename);

	}

}
