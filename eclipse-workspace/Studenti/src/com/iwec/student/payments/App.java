package com.iwec.student.payments;

import java.util.List;

import com.iwec.student.Studenti;
import com.iwec.student.reader.StudentReader;

public class App {

	public static void main(String[] args) {
		
		String fileName = "student.csv";
		StudentReader reader = new StudentReader();
		List<Studenti> students = reader.readStudents(fileName);
		
		System.out.println(students);
	}
	

}
