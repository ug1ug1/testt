package com.iwec.code_reuse.inheritance;

public class App {

	public static void main(String[] args) {
		//Monitor m = new Monitor(true, 63);
		//System.out.println(m);
		
		TV tv = new TV(false, 44, "Tera TV");
		
		tv.setContrast(88);
		System.out.println(tv);
		
		tv.doSomething();
	}

}
