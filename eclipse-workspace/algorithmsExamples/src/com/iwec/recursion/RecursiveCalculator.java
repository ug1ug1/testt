package com.iwec.recursion;

public class RecursiveCalculator {

	public int sum(int n) {
		if (n >= 1) {
			return sum(n - 1) + n;
		}
		return n;
	}

	public int tailSum(int currentSum, int n) {
		if (n <= 1) {
			return currentSum + n;
		}
		return tailSum(currentSum + n, n - 1);
	}

	public int fibonacci(int n) {
		if (n <= 1) {
			return n;
		}
		return fibonacci(n - 1) + fibonacci(n - 2);
	}

}