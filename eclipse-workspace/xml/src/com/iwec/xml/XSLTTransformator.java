package com.iwec.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XSLTTransformator {

	public static void transform(String xmlFileName, String xslFileName,
			String htmlFileName) {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tFactory
					.newTransformer(new StreamSource(xslFileName));
			transformer.transform(new StreamSource(xmlFileName),
					new StreamResult(new FileOutputStream(htmlFileName)));
		} catch (FileNotFoundException | TransformerException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}

	}

}