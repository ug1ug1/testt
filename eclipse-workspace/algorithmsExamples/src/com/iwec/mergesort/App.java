package com.iwec.mergesort;

public class App {

	public static void main(String[] args) {
		MergeSorter sorter = new MergeSorter();

		int[] array = {2, 1, 4, 6, 3, 5};
		
		for (int i : array) {
			System.out.print(i + " ");
		}

		sorter.mergeSort(array, array.length);

		System.out.println();
		for (int i : array) {
			System.out.print(i + " ");
		}
	}

}
