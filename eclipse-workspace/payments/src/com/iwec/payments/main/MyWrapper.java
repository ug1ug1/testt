package com.iwec.payments.main;

public class MyWrapper {
	public static Integer string2Int(String str) {
		Integer result = null;
		try {
			result = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			// do nothing, result remains null
		}
		return result;
	}
}

