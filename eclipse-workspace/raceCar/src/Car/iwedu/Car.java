package Car.iwedu;

public class Car {

	private String name;
	private String color;
	private Integer timePerLap;

	public Car(String name, String color, Integer timePerLap) {
		super();
		this.name = name;
		this.color = color;
		this.timePerLap = timePerLap;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getTimePerLap() {
		return timePerLap;
	}

	public void setTimePerLap(Integer timePerLap) {
		this.timePerLap = timePerLap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((timePerLap == null) ? 0 : timePerLap.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (timePerLap == null) {
			if (other.timePerLap != null)
				return false;
		} else if (!timePerLap.equals(other.timePerLap))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Car [name=" + name + ", color=" + color + ", timePerLap=" + timePerLap + "]";
	}

	public Car() {
		super();
		// TODO Auto-generated constructor stub
	}
}
