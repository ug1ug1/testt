package com.iudec.iteration;

import java.util.Arrays;
import java.util.List;

public class Iteration {

	private List<String> features = Arrays.asList("Lambdas", "Default Method",
			"Stream API", "Date and Time API");

	public void iterateAsPriorJava8() {
		for (String feature : features) {
			System.out.println(feature);
		}
	}

	public void iterateWithLambdaExperssion() {
		features.forEach(n -> System.out.println(n));
	}

	public void iterateWithMethodReference() {
		features.forEach(System.out::println);
	}

	public static void main(String args[]) {
		Iteration featuresIteration = new Iteration();

		System.out.println("\nItaration with for each loop:");
		featuresIteration.iterateAsPriorJava8();

		System.out.println("\nItaration with lambda expression:");
		featuresIteration.iterateWithLambdaExperssion();

		System.out.println("\nItaration with method reference:");
		featuresIteration.iterateWithMethodReference();
	}

}
