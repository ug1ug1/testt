package com.iwec.xml.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "ime", "prezime", "eMail" })
@XmlAccessorType(XmlAccessType.FIELD)
public class Slusatel implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement
	private String ime;

	@XmlElement
	private String prezime;

	@XmlElement(name = "e-mail")
	private String eMail;

	public Slusatel() {
		// default constructor
	}

	public Slusatel(String ime, String prezime, 
			String eMail) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.eMail = eMail;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	@Override
	public String toString() {
		return "Slusatel [ime=" + ime + ", " 
		  + "prezime=" + prezime 
		  + ", eMail=" + eMail + "]";
	}

}
