package com.iwec.binarytree;

public class App {

	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();

        tree.add(6);
        tree.add(4);
        tree.add(8);
        tree.add(3);
        tree.add(5);
        tree.add(7);
        tree.add(9);
        
        System.out.println("In-order:");
        tree.traverseInOrder(tree.getRoot());
        
        System.out.println("\nPre-order:");
        tree.traversePreOrder(tree.getRoot());

        System.out.println("\nPost-order:");
        tree.traversePostOrder(tree.getRoot());
        
        System.out.println("\nLevel-order:");
        tree.traverseLevelOrder();
        
        int x = 27;
        System.out.format("\ncontains %d: %b", x, tree.contains(x));
        

        x = 7;
        System.out.format("\ncontains %d: %b", x, tree.contains(x));
        
        tree.delete(x);
        System.out.format("\nAfter delete, contains %d: %b ->", x, tree.contains(x));
        tree.traverseInOrder(tree.getRoot());
        
        System.out.println("\nThe tree is empty: " + tree.isEmpty());
        System.out.format("The tree has %d elements.", tree.getSize());

	}

}
