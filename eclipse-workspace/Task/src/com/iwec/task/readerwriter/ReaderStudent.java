package com.iwec.task.readerwriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class ReaderStudent<T> {
	public List<T> read(String fileName, String destination) {
		List<T> result = new ArrayList<>();

		try (BufferedReader in = new BufferedReader(new FileReader(new File(fileName)));
				FileWriter out = new FileWriter(new File(destination));) {

			String line;
			while ((line = in.readLine()) != null) {
				T t = createInstance(line);
				if (t != null) {
					result.add(t);
				}
				out.write(line);;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public abstract T createInstance(String arg) ;

}
