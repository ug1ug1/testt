package com.iwec.jdbc.dao;

import java.awt.print.Book;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.iwec.jdbc.db.DB;

public class BookDAOImpl implements BookDAO {
	private static final String SQL_SELECT = "SELECT id, title, subtitle, ISBN, publish_year, edition FROM book WHERE id = ?";
	private static final String SQL_SELECT_ALL = "SELECT id, title, subtitle, ISBN, publish_year, edition FROM book";
	private static final String SQL_INSERT = "INSERT INTO book (title, subtitle, ISBN, publish_year, edition) VALUES (?, ?, ?, ?, ?)";
	private static final String SQL_DELETE = "DELETE FROM book WHERE ID = ?";
	private static final String SQL_UPDATE = "UPDATE book set title = ?, subtitle = ?, ISBN = ?, publish_year = ?, edition = ? WHERE id = ?";

	@Override
	public Book findById(Integer id) {
		if (id == null) {
			return null;
		}

		Book result = null;
		try (DB db = new DB(); PreparedStatement ps = db.getConnection().prepareStatement(SQL_SELECT);) {
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				result = new Book(rs.getInt("id"), rs.getString("title"), rs.getString("subtitle"),
						rs.getString("ISBN"), rs.getString("publish_year"), rs.getString("edition"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Book> findAll() {
		List<Book> result = new ArrayList<>();
		try (DB db = new DB();
				Statement s = db.getConnection().createStatement();
				ResultSet rs = s.executeQuery(SQL_SELECT_ALL);) {

			while (rs.next()) {
				result.add(new Book(rs.getInt("id"), rs.getString("title"), rs.getString("subtitle"),
						rs.getString("ISBN"), rs.getString("publish_year"), rs.getString("edition")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int insert(Book book) {
		if (book == null) {
			return 0;
		}

		int affected = 0;
		try (DB db = new DB(); PreparedStatement ps = db.getConnection().prepareStatement(SQL_INSERT);) {
			ps.setString(1, book.getTitle());
			ps.setString(2, book.getSubtitle());
			ps.setString(3, book.getISBN());
			ps.setString(4, book.getPublish_year());
			ps.setString(5, book.getEdition());
			affected = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return affected;
	}

	@Override
	public int deleteById(Integer id) {
		if (id == null) {
			return 0;
		}

		try (DB db = new DB(); PreparedStatement ps = db.getConnection().prepareStatement(SQL_DELETE);) {
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public int update(Book book) {
		if (book == null) {
			return 0;
		}

		int affected = 0;
		try (DB db = new DB(); PreparedStatement ps = db.getConnection().prepareStatement(SQL_UPDATE);) {
			ps.setString(1, book.getTitle());
			ps.setString(2, book.getSubtitle());
			ps.setString(3, book.getISBN());
			ps.setString(4, book.getPublish_year());
			ps.setString(5, book.getEdition());
			ps.setInt(6, book.getId());

			affected = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return affected;
	}

}
