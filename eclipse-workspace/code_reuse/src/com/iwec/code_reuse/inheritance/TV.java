package com.iwec.code_reuse.inheritance;

public class TV extends Monitor {
	private String tuner;

	public TV(boolean on, int contrast, String tuner) {
		super(on, contrast);
		this.tuner = tuner;
	}

	public String getTuner() {
		return tuner;
	}

	public void setTuner(String tuner) {
		this.tuner = tuner;
	}

	@Override
	public String toString() {
//		String s = super.toString();
//		s += String.format("tuner: %s", tuner);
//		return s;
		
		return "TV! on: " + on + " tuner: " + tuner + " contrast: " + getContrast();
	}

	@Override
	public void doSomething() {
		System.out.println("This TV does somenthing!");
	}

}
