package com.iwec.soap.repository;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.iwec.soap.model.Student;

@Component
public class StudentRepository {
	private static final Map<Integer, Student> students = new TreeMap<>();

	@PostConstruct
	public void initData() {

		Student student = new Student();
		student.setId(1);
		student.setFirstName("Круме");
		student.setLastName("Крумев");
		students.put(student.getId(), student);

		student = new Student();
		student.setId(2);
		student.setFirstName("Перо");
		student.setLastName("Перов");
		students.put(student.getId(), student);

		student = new Student();
		student.setId(3);
		student.setFirstName("Ѓургина");
		student.setLastName("Ѓурѓевна");
		students.put(student.getId(), student);

		student = new Student();
		student.setId(4);
		student.setFirstName("Paca");
		student.setLastName("Pacevskaja");
		students.put(student.getId(), student);
	}

	public Student findStudent(Integer id) {
		Assert.notNull(id, "The Student's ID must not be null.");
		return students.get(id);
	}
	
	public List<Student> getAllStudentsOrderedById() {
		
		return null;
	}
	
}