package com.iwec.cars;

public class Car {	
	
	private String manufacturer;
	private String model;
	private int prodYear;
	

	public Car(String manufacturer, String model, int prodYear) {
	   this.manufacturer = manufacturer;
	   this.model = model;
	   this.prodYear = prodYear;
	   
	}
	public String getMaunfacturer() {
		return manufacturer;
	}
	
	public void setMaunfacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public int getProdYear() {
		return prodYear;
	}
	
	public void setProdYear(int prodYear) {
		this.prodYear = prodYear;
	}
		
		
	
	
}
