package com.iwec.junit5.calc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CalculatorTest {

	@Autowired
	// given
	private Calculator calculator;

	@Test
	public void testWhenSumIsWithinIntegerBoundaries() {
		// when
		final int result = calculator.add(3, 2);

		// then
		assertEquals(5, result);
	}

	public void testWhenSumExceedsUpperIntegerBoundary() throws Exception {

		assertThrows(RuntimeException.class, () -> {
			calculator.add(Integer.MAX_VALUE, 15);
		});
	}

	public void testWhenSumExceedsLowerIntegerBoundary() throws Exception {

		assertThrows(RuntimeException.class, () -> {
			calculator.add(Integer.MIN_VALUE, -18);
		});
	}
}
