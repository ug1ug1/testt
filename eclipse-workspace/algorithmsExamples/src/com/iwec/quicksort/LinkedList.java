package com.iwec.quicksort;



public class LinkedList<T> {
	private Node<T> head;
	private int count;

	public LinkedList() {
	}

	public LinkedList(T data) {
		head = new Node<T>(data);
		count++;
	}

	public void addAtHead(T data) {
		Node<T> temp = head;
		head = new Node<T>(data);
		head.setNext(temp);
		count++;
	}
	public String toString() {
		String result = "[";
		Node<T> temp = head;
		while (temp != null) {
			result += temp.getData().toString() + ", ";
			temp = temp.getNext();
		}
		int lastIndexOfComma = result.lastIndexOf(",");
		if (lastIndexOfComma != -1) {
			result = result.substring(0, lastIndexOfComma);
		}
		return result + ']';
	}
}