package com.iwec.air;

public class AirConditioner {

	private double temperature;
	private boolean on;
	private double humidity;
	
public AirConditioner(double temperature, boolean on, double humidity) {
	
	this.temperature = temperature;
	this.on = on;
	this.humidity = humidity;
}

public double getTemperature() {
	return temperature;
}

public void setTemperature(double temperature) {
	this.temperature = temperature;
}

public boolean isOn() {
	return on;
}

public void setOn(boolean on) {
	this.on = on;
}

public double getHumidity() {
	return humidity;
}

public void setHumidity(float humidity) {
	this.humidity = humidity;
}

}
