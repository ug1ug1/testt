package com.iwec.BooksUNCHAINED;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksUnchainedApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksUnchainedApplication.class, args);
	}

}
