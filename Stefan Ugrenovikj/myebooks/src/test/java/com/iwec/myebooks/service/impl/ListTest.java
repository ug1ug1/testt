package com.iwec.myebooks.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ListTest {
	
	@Mock
	private List<Integer> brojce;
	
	@InjectMocks
	private Listt mhm;
	
	@Test
	public void TestTheFuction() {
		
		brojce.set(0, 10);
		brojce.set(1, 40);
		brojce.set(2, 30);
		
		Integer result = Listt.sum(brojce);
		
 		assertEquals((Integer) 80, result);
		
	}

}
